<?php
namespace VenusCompanion\Widgets;

use Elementor\Controls_Manager;
use Elementor\Widget_Base;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
// Exit if accessed directly

/**
 * Elementor Hero Image
 *
 * Elementor widget for Hero Image.
 *
 * @since 1.0.0
 */
class Hero_Image extends Widget_Base {

	/**
	 * Retrieve the widget name.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'hero-image';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Hero Image', 'venus-companion' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-featured-image';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * Used to determine where to display the widget in the editor.
	 *
	 * Note that currently Elementor supports only one category.
	 * When multiple categories passed, Elementor uses the first one.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return ['general'];
	}

	/**
	 * Retrieve the list of scripts the widget depended on.
	 *
	 * Used to set scripts dependencies required to run the widget.
	 *
	 * @since 1.0.0
	 *
	 * @access public
	 *
	 * @return array Widget scripts dependencies.
	 */
	/* public function get_script_depends() {
		return ['venus-companion'];
	} */

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->start_controls_section(
			'section_content',
			[
				'label' => __( 'Content', 'venus-companion' ),
			]
		);
		$this->add_control(
			'title',
			[
				'label' => __( 'Title', 'venus-companion' ),
				'type'  => Controls_Manager::TEXT,
			]
		);

		$this->add_control(
			'subheading',
			[
				'label' => __( 'SubHeading', 'venus-companion' ),
				'type'  => Controls_Manager::TEXTAREA,
			]
		);

		$this->add_control(
			'button_title',
			[
				'label'   => __( 'Button Title', 'venus-companion' ),
				'type'    => Controls_Manager::TEXT,
				'default' => __( 'About More', 'venus-companion' ),
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'section_style',
			[
				'label' => __( 'Style', 'venus-companion' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			\Elementor\Group_Control_Background::get_type(),
			[
				'name'     => 'background',
				'label'    => __( 'Background', 'venus-companion' ),
				'types'    => ['classic', 'gradient', 'video'],
				'selector' => '{{WRAPPER}} .hero-img',
			]
		);

		$this->add_control(
			'height',
			[
				'label'     => __( 'Height', 'venus-companion' ),
				'type'      => Controls_Manager::NUMBER,
				'selectors' => [
					'{{WRAPPER}} .hero-img' => 'height: {{VALUE}}px;',
				],
			],

		);

		$this->end_controls_section();
	}

	/**
	 * Render the widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

	?>
	<section class="section-gap section-top section-full">
		<div class="hero-img bg-overlay" data-overlay="5"></div>
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-7">
					<div class="bg-white- p-md-5- -p-3 -rounded text-light">
						<!-- heading -->
						<h1 class=" mb-4">
							<?php echo esc_html( $settings['title'] ); ?>
						</h1>

						<!-- subheading -->
						<p class=" mb-4">
							<?php echo esc_html( $settings['subheading'] ); ?>
						</p>

						<!-- button -->
						<p class=" mb-0">
							<a href="#" class="btn btn-pill btn-theme">
								<?php echo esc_html( $settings['button_title'] ); ?>
							</a>
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php
	}

	/**
	 * Render the widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 *
	 * @since 1.0.0
	 *
	 * @access protected
	 */
	protected function _content_template() {
		?>
		<div class="title">
			{{{ settings.title }}}
		</div>
		<?php
	}
}
